package com.digitalpaper.utils.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

import com.recoveryportal.constants.core.ApplicationConstants;

@Component
public class TimeZoneProvider {
	private TimeZoneProvider() {
		/**/}

	private static Map<String, String> timeZone = new HashMap<>();

	static {
		timeZone.put("IST", "Asia/Calcutta");
		timeZone.put("UTC", "UTC");
		timeZone.put("EST", "America/New_York");
		timeZone.put("CST", "America/Havana");
		timeZone.put("GMT", "Asia/Bahrain");
		timeZone.put("GST", "Asia/Dubai");
	}

	public static String getConventionalTimeZone(String timeZoneStr) {
		return timeZone.get(timeZoneStr) == null ? timeZoneStr : timeZone.get(timeZoneStr);
	}

	public static String convertIstToUtc(String date) throws Exception {
		Date date1 = convertStringToDate(date,ApplicationConstants.DEFAULT_DATE_FORMAT);
		SimpleDateFormat Formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		String utcTime = Formatter.format(date1);
		return utcTime;

	}	

	
	
	public static String convertUtcToIst(String date) throws Exception {
		Date date1 = convertStringToDate(date,ApplicationConstants.DEFAULT_DATE_FORMAT);
		SimpleDateFormat Formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Formatter.setTimeZone(TimeZone.getTimeZone("IST"));
		String IstTime = Formatter.format(date1);
		return IstTime;

	}

	public static Date convertStringToDate(String date,String format) throws Exception {
		  SimpleDateFormat formatter=new SimpleDateFormat(format);  
		Date date1 = formatter.parse(date);
		return date1;
	}
	
	public static LocalDateTime convertStringToLocalDateTime(String date,String format) throws Exception{
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format); 
	LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
	return dateTime;
	}
	
	public static String convertDateToString(Date date,String format)throws Exception{
		DateFormat dateFormat = new SimpleDateFormat(format);  
		String strDate = dateFormat.format(date); 
		return strDate;
		
	}
}
