package com.recoveryportal.transfer.object.notification.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.digitalpaper.transfer.object.reportloss.entity.Company;
import com.digitalpaper.transfer.object.reportloss.entity.ReportLoss;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name="notification_history")
@NoArgsConstructor
public class NotificationHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "notification_history_id")
	private int id;	
	  
	@Column(name = "notification_template")
	private String template;
	
	@Column(name = "is_read")
	private boolean isRead;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "claim_id")
	private ReportLoss claimDetails;
	
	@Column(name = "status")
	private String status;
	
	@OneToOne
	@JoinColumn(name = "last_acted")
	private Company lastActed;
	
	@OneToOne
	@JoinColumn(name = "to_notify")
	private Company toNotify;
	
	@Column(name="is_deleted")
	private boolean isDeleted;

	@Column(name="identity")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String identity;

	@Column(name="created_date")
	private Date createdDate;

	@Column(name="created_by")
	private int createdBy;
	
	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="modified_by")
	private int modifiedBy;

}
