package com.recoveryportal.transfer.object.entity;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.digitalpaper.transfer.object.reportloss.entity.DropdownOptions;
import com.digitalpaper.transfer.object.reportloss.entity.ReportLoss;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "total_loss")
@Data
@NoArgsConstructor
public class TotalLoss {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="claim_id")
	private ReportLoss reportLoss;
	
	@Column(name="adjustor_name1")
	private String adjustorName1;
	
	@Column(name="adjustor_name2")
	private String adjustorName2;
	
	@Column(name="survey_date1")
	private LocalDate surveyDate1;
	
	@Column(name="survey_date2")
	private LocalDate surveyDate2;
	
	@Column(name="total_loss_amount1")
	private Double totalLossAmount1;
	
	@Column(name="total_loss_amount2")
	private Double totalLossAmount2;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="reason_for_total_loss")
	private DropdownOptions reasonForTotalLoss;
	
	@Column(name="estimated_total_loss_amount")
	private Double estimatedTotalLossAmount;
	
	@Column(name="salvage_seller_name")
	private String salvageSellerName;
	
	@Column(name="salvage_amount")
	private Double salvageAmount;
	
	@Column(name="salvage_buyer_name")
	private String salvageBuyerName;
	
	@Column(name="is_deleted")
	private boolean isDeleted;
	
	@Column(name="identity")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String identity;

	@Column(name="created_date")
	private LocalDate createdDate;

	@Column(name="created_by")
	private int createdBy;
	
	@Column(name="modified_date")
	private LocalDate modifiedDate;

	@Column(name="modified_by")
	private int modifiedBy;
	
}
