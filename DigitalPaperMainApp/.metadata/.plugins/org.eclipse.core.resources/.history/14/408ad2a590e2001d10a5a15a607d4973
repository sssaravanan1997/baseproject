/*
 * @author codeboard
 */
package com.digitalpaper.config.infrastructure;

import java.util.Date; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaper.constants.core.ApplicationConstants;
import com.recoveryportal.exception.core.ApplicationException;
import com.recoveryportal.exception.core.codes.ErrorCodes;
import com.recoveryportal.utils.core.ApplicationDateUtils;



/**
 * The Class DataTypeConvertor.
 */
@Component
public class DataTypeConvertor {

	/**
	 * The application data util.
	 */
	@Autowired
	private ApplicationDateUtils applicationDataUtil;
	/**
	 * The logger.
	 */
	protected Logger logger = LoggerFactory.getLogger(DataTypeConvertor.class);

	/**
	 * Conver to real data type.
	 *
	 * @param <Y>  the generic type
	 * @param data the data
	 * @param type the type
	 * @return the y
	 * @throws ApplicationException the application exception
	 */
	@SuppressWarnings("unchecked")
	public <Y extends Comparable<? super Y>> Y converToRealDataType(String data, String type)
			throws ApplicationException {

		if (data == null || data.trim().isEmpty()) {
			data = "";
			return (Y) data;
		}

		data = data.trim();

		if (type.equals(ApplicationConstants.FILTER_TYPE_INTEGER)) {
			try {
				return (Y) Integer.valueOf(data);
			} catch (Exception e) {
				throw new ApplicationException(ErrorCodes.INVALID_DATA);
			}
		} else if (type.equals(ApplicationConstants.FILTER_TYPE_BOOLEAN)) {
			try {
				return (Y) Boolean.valueOf(data);
			} catch (Exception e) {
				throw new ApplicationException(ErrorCodes.INVALID_DATA);
			}
		} else if (type.equals(ApplicationConstants.FILTER_TYPE_DOUBLE)) {
			try {
				return (Y) Double.valueOf(data);
			} catch (Exception e) {
				throw new ApplicationException(ErrorCodes.INVALID_DATA);
			}
		} else if (type.equals(ApplicationConstants.FILTER_TYPE_TEXT)) {
			return (Y) data.trim();
		} else if (type.equals(ApplicationConstants.FILTER_TYPE_DATE)) {
			Date date = applicationDataUtil.convertStrIntoDate(data, ApplicationConstants.DEFAULT_FULL_DATE_FORMAT,
					ApplicationDateUtils.UTC_TIMEZONE);
			return (Y) date;
		} else {
			throw new ApplicationException(ErrorCodes.INVALID_DATA);
		}

	}

}
